<?php

return [

    'name'=>1,
    'targetFor2020'=> 'текст',
    'compositionOfObject' => [
        'Здание…',
        'Сооружение…',
        'Часть здания ….',
        'Текст…',
    ],
    'availabilityOfTitleDocumentsForLandPlot'=>[
        'cadastralNumberOfLandPlot' => 00000000000,
        'numberOfFreeUseAgreement' => 000000,
        'dateOfFreeUseAgreement' => '01.01.2020',
        'numberOfSupplementaryAgreement' => 0000,
        'dateOfSupplementaryAgreement' => '01.01.2020',
        'numberOfGPZY' => 00000000,
        'dateOfGPZY' => '01.01.2020',
        'dateOfTheCadastralStatementLandPlot' => '01.01.2020',
        'numberOfTheCadastralStatementLandPlot' => 000000,
        'cadastralNumber' => '76:20:010106:2',
    ],
    'percentageOfDesign&SurveyWork'=> 000,
    'progressOfDesign&SurveyWork'=>[
        'firstTechnicalCondition'=>[
            'number' => 0000000,
            'date' => '01.01.2020',
        ],
    ],
    'presenceOfConclusionsStateExpertise'=>[
        'projectDocumentation'=> [
            'number' => 0000000000,
            'date' => '01.01.2020',
            'percentage' => 000,
        ],
        'estimateDocumentation'=> [
            'number' => 0000000000,
            'date' => '01.01.2020',
            'percentage' => 000,
        ],
    ],
    'progressWorkingDocumentationDevelopment'=>[
        'phaseOne' => 'текст….',
        //...
    ],
    'buildingPermit'=>[
        'RNSphaseOne'=>[
            'number' => 0000000,
            'dateFrom' => '01.01.2020',
            'dateTo' => '01.01.2020',
        ],
    ],
    'progressOfConstruction&InstallationWorks'=>[
        'date' => '01.01.2020',
        'numberOfPeople' => 00,
        'numberOfEquipment' => 00,
        'dynamicForWeekOfNumberOfPeople' => 00,
        'dynamicForWeekOfNumberOfEquipment' => 00,
        'needToAttractPeople' => 00,
        'needToAttractEquipment' => 00,
        'firstStageOfBuilding'=>[
            'dateOfStartOfWorks' => '01.01.2020',
        ],
        //...
        'percentageOfReadiness' => 000,
        'dynamicPercentageOfReadinessForWeek' => 00,
    ],
    'performWorks'=>[
        'description' => 'текст',
        'problematicIssues' => 'текст',
        'waysOfSolution' => 'текст',
        'lackOfMaterials' => 'текст',
    ],
];