<?php


namespace Classes;


class Contract
{
    protected $array = [];

    /**
     * Contract constructor.
     * @param array $array Data for the Contract
     */
    public function __construct(array $array)
    {
        $this->array = $array;
    }

    /**
     * @return string
     */
    public function printName($i){
        return $print = $i.'. Государственный контракт № ' . $this->array['number'] . ' от ' . $this->array['date'] . ' г.';
    }

    /**
     * @return string
     */
    public function printHeader($i){
        $print = [
            '1' => 'Сроки окончания работ по ГК:',
        ];
        return $print[$i];
    }

    /**
     * @return string[]
     */
    public function printPart($i)
    {
        $print = [
            '0' => [
                'Исполнитель – ООО «'.$this->array['executor'].'»',
                'Генподрядчик – ООО «'.$this->array['generalContractor'].'»',
                'Цена государственного контракта – '.$this->array['priceOfTheStateContract'].' тыс. руб.',
                'Генеральный проектировщик – ООО «' . $this->array['genProjector'] . '»',
                'Субподрядчики по СМР – ООО «'.$this->array['subcontractorsCMP'].'»',
                'Субподрядчик по ПИР – ООО «' . $this->array['subContractorPIR'] . '»',
                'Цена государственного контракта – ' . $this->array['priceOfStateContract'] . 'тыс. руб.',
                'Оплачено по ГК – '.$this->array['paidByGK'].' тыс. руб.',
                'Принято работ на сумму – '.$this->array['acceptedWorksFor'].' тыс. руб.',
                'Дебиторская задолженность – '.$this->array['receivables'].' тыс. руб.',
                'Кредиторская задолженность – '.$this->array['creditorIndebtedness'].' тыс. руб.',
                'План финансирования на 2020г. – '.$this->array['financingPlanFor2020'].' тыс. руб.',
                'Выполнено работ по форме КС-6а, но не предъявлено – '.$this->array['completedWorkFormKS6ANotPresented'].' тыс. руб.',
                'Зарегистрировано бюджетных обязательств – на 2020г. – '.$this->array['registeredFor2020BudgetCommitments'].'  тыс. руб., на 2021г. – '.$this->array['registeredFor2021BudgetCommitments'].'  тыс. руб., на 2022г. – '.$this->array['registeredFor2022BudgetCommitments'].'  тыс. руб.,',
                '',
                'Профинансировано в 2020 году - '.$this->array['fundedIn2020'].' тыс. руб.',
                '',
            ],
            '1' => [
                '- Окончание выполнения обследований – до 01.01.2020 г.;',
                '- Окончание строительно-монтажных работ – до 01.01.2020 г.;',
                '- Подписание итогового акта приемки выполненных работ – до 01.01.2020 г.;',
                '',
            ],
        ];
        return $print[$i];
    }
}