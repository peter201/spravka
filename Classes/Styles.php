<?php


namespace Classes;


abstract class Styles
{
    public static $authorFontStyle = array('italic'=>true);
    public static $authorParStyle = array('alignment' => 'right');
    public static $underlinedTitleFontStyle = array('underline' => 'single', 'bold' => true);
    public static $mainPartFontStyle = array('bold'=>true);
    public static $mainPartParStyle = array('alignment' => 'left','indent' => 1);
    public static $listFontStyle = array();
    public static $titleFontStyle = array('bold' => true);
    public static $titleParStyle = array('alignment' => 'center','indent' => 1);

}