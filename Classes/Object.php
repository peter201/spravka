<?php


namespace Classes;


class Object
{
    protected $array = [];

    /**
     * Contract constructor.
     * @param array $array Data for the Contract
     */
    public function __construct(array $array)
    {
        $this->array = $array;
    }
    /**
     * @return string
     */
    public function printHeader($i){
        $print = [
            '0' => 'Справка-доклад по объекту: '.$this->array['name'],
            '1' => 'Целевая задача на 2020г.:',
            '2' => 'Состав объекта:',
            '3' => 'Наличие правоустанавливающих документов на земельный участок:',
            '4' => 'Ход проведения проектно-изыскательских работ ('.$this->array['percentageOfDesign&SurveyWork'].' %):',
            '5' => 'Ход разработки рабочей документации:',
            '6' => 'Разрешение на строительство:',
            '7' => 'Ход выполнения строительно-монтажных работ:',
            '8' => 'Выполняемые работы:',
        ];
        return $print[$i];
    }
    /**
     * @return string[]
     */
    public function printPart($i)
    {
        $print =
            [   '0' => [
                '',
                '',
                '',
                '',
                '',
                ],
                '1' => [
                $this->array['targetFor2020'],
                '',
                ],
                '2' => [
                    'В соответствии с ГК предусмотрено:',
                    $this->array['compositionOfObject'],
                    '',
                ],
                '3' => [
                    'Земельный участок с кадастровым номером № '.$this->array['availabilityOfTitleDocumentsForLandPlot']['cadastralNumberOfLandPlot'],
                    '- договор безвозмездного пользования земельным участком № '.$this->array['availabilityOfTitleDocumentsForLandPlot']['numberOfFreeUseAgreement'].' от '.$this->array['availabilityOfTitleDocumentsForLandPlot']['dateOfFreeUseAgreement'].' (дополнительное соглашение № '.$this->array['availabilityOfTitleDocumentsForLandPlot']['numberOfSupplementaryAgreement'].' от '.$this->array['availabilityOfTitleDocumentsForLandPlot']['dateOfSupplementaryAgreement'].' г.)',
                    '- ГПЗУ № '.$this->array['availabilityOfTitleDocumentsForLandPlot']['numberOfGPZY'].' , утвержденным приказом руководителя '.$this->array['availabilityOfTitleDocumentsForLandPlot']['dateOfGPZY'],
                    '- Кадастровая выписка о земельном участке от '.$this->array['availabilityOfTitleDocumentsForLandPlot']['dateOfTheCadastralStatementLandPlot'].' № '.$this->array['availabilityOfTitleDocumentsForLandPlot']['numberOfTheCadastralStatementLandPlot'].' кадастровый номер '.$this->array['availabilityOfTitleDocumentsForLandPlot']['cadastralNumber'],
                    '',
                    '',
                ],
                '4' => [
                    'Технические условия:',
                    '1. ТУ № '.$this->array['progressOfDesign&SurveyWork']['firstTechnicalCondition']['number'].' от '.$this->array['progressOfDesign&SurveyWork']['firstTechnicalCondition']['date'],
                    '',
                    'Наличие заключений государственной экспертизы:',
                    'Проектная документация',
                    '№ '.$this->array['presenceOfConclusionsStateExpertise']['projectDocumentation']['number'].' от '.$this->array['presenceOfConclusionsStateExpertise']['projectDocumentation']['date'].' –  '.$this->array['presenceOfConclusionsStateExpertise']['projectDocumentation']['percentage'].' %',
                    '',
                    'Сметная документация',
                    '№ '.$this->array['presenceOfConclusionsStateExpertise']['estimateDocumentation']['number'].' от '.$this->array['presenceOfConclusionsStateExpertise']['estimateDocumentation']['date'].' –  '.$this->array['presenceOfConclusionsStateExpertise']['estimateDocumentation']['percentage'].' %',
                    '',
                ],
                '5' => [
                    '1 этап – '.$this->array['progressWorkingDocumentationDevelopment']['phaseOne'],
                    '',
                ],
                '6' => [
                    '1 этап РНС № '.$this->array['buildingPermit']['RNSphaseOne']['number'].' от '.$this->array['buildingPermit']['RNSphaseOne']['dateFrom'].' г., срок действия до '.$this->array['buildingPermit']['RNSphaseOne']['dateTo'],
                    '',
                ],
                '7' => [
                    'По состоянию на '.$this->array['progressOfConstruction&InstallationWorks']['date'].' на объекте работают: '.$this->array['progressOfConstruction&InstallationWorks']['numberOfPeople'].' чел. / '.$this->array['progressOfConstruction&InstallationWorks']['numberOfEquipment'].' ед. тех. (динамика за неделю: + '.$this->array['progressOfConstruction&InstallationWorks']['dynamicForWeekOfNumberOfPeople'].' чел., + '.$this->array['progressOfConstruction&InstallationWorks']['dynamicForWeekOfNumberOfEquipment'].' ед. тех.)',
                    'В соответствии с детализированным графиком производства работ необходимо привлекать '.$this->array['progressOfConstruction&InstallationWorks']['needToAttractPeople'].' человек и '.$this->array['progressOfConstruction&InstallationWorks']['needToAttractEquipment'].' единиц строительной техники. ',
                    '',
                    '1 этап строительства: ',
                    'По утвержденному ГПР разворот работ спланирован на '.$this->array['progressOfConstruction&InstallationWorks']['firstStageOfBuilding']['dateOfStartOfWorks'].' (работы развернуты)',
                    '',
                    'Общий (средний) процент строительной готовности объекта '.$this->array['progressWorkingDocumentationDevelopment']['percentageOfReadiness'].' % (динамика за неделю + '.$this->array['progressWorkingDocumentationDevelopment']['dynamicPercentageOfReadinessForWeek'].' %).',
                    '',
                ],
                '8' => [
                    $this->array['performWorks']['description'],
                    '',
                    'Проблемные вопросы:',
                    $this->array['performWorks']['problematicIssues'],
                    '',
                    'Пути решения:',
                    $this->array['performWorks']['waysOfSolution'],
                    '',
                    'Дефицит материалов:',
                    $this->array['performWorks']['lackOfMaterials'],
                    '',
                    '',
                ],

        ];
        return $print[$i];
    }
}
