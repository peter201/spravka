<?php
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/Classes/Contract.php';
require_once __DIR__.'/Classes/Object.php';
require_once __DIR__.'/Classes/Styles.php';

$phpWord = new \PhpOffice\PhpWord\PhpWord();

$phpWord->setDefaultFontName('Times New Roman');

$phpWord->setDefaultFontSize(14);

$sectionStyle = array(
    'marginTop'=> \PhpOffice\PhpWord\Shared\Converter::cmToTwip(0.75),
    'marginBottom'=> \PhpOffice\PhpWord\Shared\Converter::cmToTwip(0.75),
    'marginLeft'=> \PhpOffice\PhpWord\Shared\Converter::cmToTwip(1),
    'marginRight'=> \PhpOffice\PhpWord\Shared\Converter::cmToTwip(1),
);

$section = $phpWord->addSection($sectionStyle);

use Classes\Object;
use Classes\Contract;

$numberOfContracts = 3;
$numberOfSectionsInObject = 8;
$obj = new Object(require __DIR__ . '/Data/dataForObjects.php');
$contract1 = new Contract(require __DIR__ . '/Data/dataForContracts.php');
$contract2 = new Contract(require __DIR__ . '/Data/dataForContracts.php');
$contract3 = new Contract(require __DIR__ . '/Data/dataForContracts.php');

$authors = [
    'Подготовлено: ',
    '',
];

foreach ($authors as $author) {
    $section->addText($author,\Classes\Styles::$authorFontStyle,\Classes\Styles::$authorParStyle);
}

$section->addText($obj->printHeader(0), \Classes\Styles::$titleFontStyle,\Classes\Styles::$titleParStyle);

$section->addText('', \Classes\Styles::$titleFontStyle,\Classes\Styles::$titleParStyle);
for ($i=1; $i<$numberOfContracts+1; $i++){
    $contractName = 'contract'.$i;

    $section->addText($$contractName->printName($i), \Classes\Styles::$underlinedTitleFontStyle,\Classes\Styles::$mainPartParStyle);

        foreach ($$contractName->printPart(0) as $row){
            $section->addText($row, \Classes\Styles::$listFontStyle,\Classes\Styles::$mainPartParStyle);
        }

        $section->addText($$contractName->printHeader(1), \Classes\Styles::$underlinedTitleFontStyle,\Classes\Styles::$mainPartParStyle);

        foreach ($$contractName->printPart(1) as $row){

            $section->addText($row, \Classes\Styles::$listFontStyle,\Classes\Styles::$mainPartParStyle);

    }
}

foreach ($obj->printPart(0) as $row) {

    $section->addText($row, \Classes\Styles::$mainPartFontStyle,\Classes\Styles::$listFontStyle);

}

for ($i=1; $i<$numberOfSectionsInObject+1; $i++){

    $section->addText($obj->printHeader($i), \Classes\Styles::$underlinedTitleFontStyle,\Classes\Styles::$mainPartParStyle);

    foreach ($obj->printPart($i) as $row) {

        $section->addText($row, \Classes\Styles::$listFontStyle,\Classes\Styles::$mainPartParStyle);

    }

}

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord,'Word2007');
$objWriter->save('spravka.docx');


